//全域變數
var gv = gv || {};
gv.minW=1000;//最小寬
gv.minH=600;//最小高
var nowPath;
jQuery.fn.hashchange.domain = document.domain;


$(document).ready( function() {
	$('body').jpreLoader({
		splashID: "#jSplash",
		loaderVPos: '50%',
		autoClose: true
	},function() {
		Resize();
		startgo();
	});

    window.onresize = Resize;
    $(window).hashchange(addressChange);

});




function startgo(){

	$('.work_hit').hover(function() {
		TweenMax.to($(this),0.3, { opacity:0.4,ease:Linear.easeNone})
	}, function() {
		TweenMax.to($(this),0.3, { opacity:0,ease:Linear.easeNone})  
	});


	$('#topicon').click(function(){
 		$("#container").animate({scrollTop:0},450);
	});

	$('#test').css('display','none');

	$('.work_hit').bind('click',work_hitClick);

	$('#close').on('click',function(e){
		e.preventDefault();
		// $('body').css({overflow:'auto'});
		nowPath="";
		location.hash = nowPath;
		$('#close').css('display','none');
		$("#test")
			.css({top:95,position:'fixed'})
			.animate({top:1500}, 500, function() {
				//callback
				checkContainerScroll();
			}).fadeOut();
	});

	$( "#container" ).scroll(checkContainerScroll);
	addressChange();
}


function checkContainerScroll(){
	var conScrolVal=$( "#container" ).scrollTop();

	if(conScrolVal==0){
		logoSizeChange(0);

	}else{
		logoSizeChange(1);
	}
}


function work_hitClick(e){
	e.preventDefault();
	var target = $(e.target).attr('href');
	var closeC = $(e.target).attr('closeC');
	// $('body').css({overflow:'hidden'});

	nowPath=target;
	location.hash=nowPath;
	$('#testFrame').attr('src',target+".html");

	$("#test").fadeIn();
	$("#test").css({top:1500,position:'fixed',width:"100%"})
	$("#test").animate({top:95}, 800, function() {
			if(closeC==1){
				$('#close').css('background-position','top');
			}else{
				$('#close').css('background-position','bottom');
			}
				
			$('#close').css('display','block');
	});

	TweenMax.delayedCall(1,logoSizeChange,[1]);
	// logoSizeChange(1);
}





function logoSizeChange(n){
	if(n==0){//放大
		TweenMax.to($( "#nav_logo" ),0.5,{
	    	opacity: 1,
	    	left: 415,
	    	top:0,
	  	});

		TweenMax.to($( "#nav_logo img" ),0.5,{
	 		width:147,
	 		height:230,
	  	});

	  	TweenMax.to($( "#header" ),0.5,{
	 		height:165
	  	});

	  	TweenMax.to($( "#header .nav" ),0.5,{
	 		top:40,

	  	});

	  	TweenMax.to($( "#container" ),0.5,{
	 		top:165,
	 		height:$(window).height()-165
	  	});


	}else if(n==1){//縮小
		TweenMax.to($( "#nav_logo" ),0.4,{
	    	opacity: 1,
	    	left: 432,
	    	top:-25,
	  	});

		TweenMax.to($( "#nav_logo img" ),0.4,{
	 		width:"75%",
	 		height:"75%"
	  	});

	  	TweenMax.to($( "#header" ),0.5,{
	 		height:107
	  	});

	  	TweenMax.to($( "#header .nav" ),0.4,{
	 		top:-7,
	  	});

	  	TweenMax.to($( "#container" ),0.4,{
	 		top:107,
	 		height:$(window).height()-107
	  	});
	}
	

}


function Resize() {
	gv.winW = $(window).width(); //視窗的寬
	gv.winH = $(window).height(); //視窗的高


	

	if(gv.winW < gv.minW){
		$('#container').css('width',1000);

    }else{
    	$('#container').css('width',gv.winW);

    };


	var headerH=$('#header').height();

	if(gv.winH < gv.minH){
		$('#container').css('height',600-headerH);

    }else{
    	$('#container').css('height',gv.winH-headerH);

    };
    $('iframe').css('height',gv.winH-90);
}


function addressChange(){
	nowPath = location.hash.replace("#","");

	$('a.work_hit').each(function() {
		if ($( this ).attr('href')==nowPath) {
			$( this ).click();
		};
	});

}