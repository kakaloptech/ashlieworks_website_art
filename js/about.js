//全域變數
var gv = gv || {};
gv.minW=1000;//最小寬
gv.minH=600;//最小高


$(document).ready( function() {
	$('body').jpreLoader({
		splashID: "#jSplash",
		loaderVPos: '50%',
		autoClose: true
	},function() { 
		Resize();
    	startgo();
	});

	window.onresize = Resize;

});

function startgo(){

	$('#topicon').click(
		function(){

 		$("#container").animate({scrollTop:0},450);
	});

	$( "#container" ).scroll(checkContainerScroll);
}

function checkContainerScroll(){
	var conScrolVal=$( "#container" ).scrollTop();

	if(conScrolVal==0){
		logoSizeChange(0);

	}else{
		logoSizeChange(1);
	}
}




function logoSizeChange(n){
	if(n==0){//放大
		TweenMax.to($( "#nav_logo" ),0.5,{
	    	opacity: 1,
	    	left: 415,
	    	top:0,
	  	});

		TweenMax.to($( "#nav_logo img" ),0.5,{
	 		width:147,
	 		height:230,
	  	});

	  	TweenMax.to($( "#header" ),0.5,{
	 		height:165
	  	});

	  	TweenMax.to($( "#header .nav" ),0.5,{
	 		top:40,

	  	});

	  	TweenMax.to($( "#container" ),0.5,{
	 		top:165,
	 		height:$(window).height()-165
	  	});


	}else if(n==1){//縮小
		TweenMax.to($( "#nav_logo" ),0.4,{
	    	opacity: 1,
	    	left: 432,
	    	top:-25,
	  	});

		TweenMax.to($( "#nav_logo img" ),0.4,{
	 		width:"75%",
	 		height:"75%"
	  	});

	  	TweenMax.to($( "#header" ),0.5,{
	 		height:107
	  	});

	  	TweenMax.to($( "#header .nav" ),0.4,{
	 		top:-7,
	  	});

	  	TweenMax.to($( "#container" ),0.4,{
	 		top:107,
	 		height:$(window).height()-107
	  	});
	}
	

}


function Resize() {
	gv.winW = $(window).width(); //視窗的寬
	gv.winH = $(window).height(); //視窗的高

	if(gv.winW < gv.minW){
		$('#container').css('width',1000);

    }else{
    	$('#container').css('width',gv.winW);

    };


	var headerH=$('#header').height();

	if(gv.winH < gv.minH){
		$('#container').css('height',600-headerH);

    }else{
    	$('#container').css('height',gv.winH-headerH);

    };

}